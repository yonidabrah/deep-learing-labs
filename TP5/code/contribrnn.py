import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np

'''
-------------------------------------------------------------------------
Données MNIST
-------------------------------------------------------------------------
''' 

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("../data/mnistdata", one_hot=True)

num_examples = mnist.train.images.shape[0] 
num_input = mnist.train.images.shape[1]
num_classes = mnist.train.labels.shape[1]

''' 
-------------------------------------------------------------------------
Paramètres du réseau
-------------------------------------------------------------------------
'''
num_hidden    = 28
learning_rate = 0.001
num_epochs    = 100
batch_size    = 128
display       = 10

#images de taille 28*28 : à transformer en 28 séquences de 28 éléments
num_input = 28 
num_steps = 28

# configuration
#                        O * W + b -> 10 classes pour chaque image, O[? 28], W[28 10], B[10]
#                       ^ (O: sortie 28 vec à partir de 28 vec d'entrée)
#                       |
#      +-+  +-+       +--+
#      |1|->|2|-> ... |28| num_steps = 28
#      +-+  +-+       +--+
#       ^    ^    ...  ^
#       |    |         |
# img1:[28] [28]  ... [28]
# img2:[28] [28]  ... [28]
# img3:[28] [28]  ... [28]
# ...
# img128 (batch_size 128)
#      chaque taille d'entrée =  num_input = 28

x = tf.placeholder("float", [None, num_steps, num_input])
y = tf.placeholder("float", [None, num_classes])

# Poids du réseau
weights = {'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))}
biases = {'out': tf.Variable(tf.random_normal([num_classes]))}

'''
Définition du réseau récurrent en utilisant les fonctions TensorFlow
'''

def RNN(x, weights, biases):

    '''Transformation des données pour satisfaire aux entrées de rnn
    Il faut construire une liste de num_steps tenseurs de taille (batch_size, num_input)
    A partir de tenseurs (batch_size, num_steps, num_input)
    ''' 

    # X, input shape: (batch_size, num_steps, num_input)
    x = tf.transpose(x, [1, 0, 2])     # permute num_steps et batch_size
    # X shape: (num_steps, batch_size, num_input)
    x = tf.reshape(x, [-1, num_input]) # chaque ligne a une entrée pour chaque cellule lstm (lstm_size = num_input)
    # X shape: (num_steps * batch_size, num_input)
    x = tf.split(x, num_steps, 0)      # le séparer en num_steps (28 arrays)
    # Taille des arrays: (batch_size, num_input)

    # Cellule LSTM avec une taille num_hidden
    lstm_cell = rnn.BasicLSTMCell(num_hidden, forget_bias=1.0)

    # Récupération de la sortie de la cellule LSTM: Arrays de taille num_steps avec des sorties de taille num_hidden: (batch_size, num_hidden)
    outputs, states = rnn.static_rnn(lstm_cell, x, dtype=tf.float32)

    # Activation linéaire sur la sortie
    return tf.matmul(outputs[-1], weights['out']) + biases['out']

#Modèle
pred = RNN(x, weights, biases)

# Fonction de perte et procédure d'optimisation
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Evaluation du modèle
correct_pred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialisation des variables
init = tf.global_variables_initializer()

# Création d'une session TF pour exécuter le programme
with tf.Session() as sess:
    sess.run(init)

    # Entraînement
    total_batch = int(num_examples/batch_size)
    for epoch in range(num_epochs):
        # Entraînement sur les batchs d'images
        for step in range (total_batch):
            perm = np.arange(num_examples)
            np.random.shuffle(perm)
            indices = perm[0:batch_size]

            batch_x = mnist.train.images[indices]
            batch_x = batch_x.reshape((batch_size, num_steps, num_input))
            batch_y = mnist.train.labels[indices]

            sess.run(optimizer, feed_dict={x: batch_x, y: batch_y})

            if step % display == 0:
                loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x,y: batch_y})
                print("Iteration " + str(epoch * total_batch + step) + ", Précision  = " + "{:.5f}".format(acc))

    # Données de test
    test_len = 128
    test_data = mnist.test.images[:test_len].reshape((-1, num_steps, num_input))
    test_label = mnist.test.labels[:test_len]
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={x: test_data, y: test_label}))
